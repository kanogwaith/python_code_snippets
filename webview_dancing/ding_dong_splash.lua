--
-- Created by IntelliJ IDEA.
-- User: vasily
-- Date: 10.12.2019
-- Time: 10:29
-- To change this template use File | Settings | File Templates.
--

function main(splash, args)
    splash:set_viewport_size(1600, 900)
    local results = {}
    local ok, reason = splash:go(args.url)
    splash:set_viewport_full()
    if ok then
        results[args.url] = splash:png()
    end
    return results
end
