import sys
import time
from PyQt5.QtCore import QUrl
from PyQt5.QtGui import QPainter, QImage
from PyQt5.QtWebKitWidgets import QWebView
from PyQt5.QtWidgets import QApplication


class Screenshotter(QWebView):
    def __init__(self):
        self.app = QApplication(sys.argv)
        QWebView.__init__(self)
        self._loaded = False
        self.loadFinished.connect(self._loadFinished)
        self.setFixedSize(1600, 900)

    def capture(self, url, output_file):
        self.load(QUrl(url))
        self.wait_load()
        # set to webpage size
        frame = self.page().mainFrame()
        self.page().setViewportSize(frame.contentsSize())
        # render image
        image = QImage(self.page().viewportSize(), QImage.Format_ARGB32)
        painter = QPainter(image)
        frame.render(painter)
        painter.end()
        print('saving', output_file)
        image.save(output_file)

    def wait_load(self, delay=0):
        # process app events until page loaded
        while not self._loaded:
            self.app.processEvents()
            time.sleep(delay)
        self._loaded = False

    def _loadFinished(self, result):
        self._loaded = True

s = Screenshotter()
s.capture('https://lifehacker.ru/samye-populyarnye-seks-igrushki/', 'website3.png')
