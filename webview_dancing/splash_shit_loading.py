import requests
import pymongo
from threading import Thread
from queue import Queue
import logging

logging.basicConfig(
    format=u'%(filename)s[LINE:%(lineno)d]# %(levelname)-8s  %(threadName)s [%(asctime)s]  %(message)s',
    level=logging.DEBUG
)
logger = logging.getLogger(__name__)

script = """
splash:set_viewport_size(1600, 900)
local results = {}
local ok, reason = splash:go(args.url)
splash:set_viewport_full()
return splash:png()
"""


def listener(que):

    while True:
        _id, url = que.get()

        if _id is None:
            logger.warn('STOPPED')
            break

        resp = requests.post('http://localhost:8050/run', json={
            'lua_source': script,
            'url': url
        })
        print(resp.content)
        with open('images/{}.png'.format(_id), 'wb') as f:
            f.write(resp.content)
            logger.info('_id {} is saved'.format(_id))


queue = Queue(20)

for i in range(4):
    worker = Thread(target=listener, args=(queue,))
    worker.setDaemon(True)
    worker.start()

mongo = pymongo.MongoClient("mongodb://green101:37017,green102:37017,green103:37017")
coll = mongo['imageIndex']['ImageIndex']

for i in coll.find({}, no_cursor_timeout=True):
    if i.get('PageUrls'):
        queue.put((str(i['_id']), i['PageUrls'][0]))



