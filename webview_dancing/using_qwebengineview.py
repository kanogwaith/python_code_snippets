import sys
from PyQt5.QtWidgets import QApplication
from PyQt5.QtCore import QUrl, QPoint
from PyQt5.QtGui import QPaintDevice, QPainter, QRegion, QImage
from PyQt5.QtWebEngineWidgets import QWebEngineView


app = QApplication(sys.argv)
web = QWebEngineView()

web.load(QUrl("https://stackoverflow.com/questions/48166021/how-to-take-webpage-screenshots-using-qwebengineview-without-opening-window"))
web.show()
web.setFixedSize(web.page().contentsSize().toSize())

img = QImage(web.page().contentsSize().toSize(), QImage.Format_ARGB32)
painter = QPainter(img)

web.render(painter)
painter.end()

img.save("some.png")

sys.exit(app.exec_())
