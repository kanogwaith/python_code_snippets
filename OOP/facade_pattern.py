class EventManager(object):

    def __init__(self):
        self.hotelier = None
        self.florist = None
        self.caterer = None
        self.musician = None

        print('Event Manager:: Let me talk to hte folks\n')

    def arrange(self):
        self.hotelier = Hotelier()
        self.hotelier.book_hotel()

        self.florist = Florist()
        self.florist.set_flower_requirements()

        self.caterer = Caterer()
        self.caterer.set_cuisine()

        self.musician = Musician()
        self.musician.set_music_type()


class Hotelier(object):

    def __init__(self):
        print('Arranging the Hotel for Marriage? --')

    @staticmethod
    def __is_available():
        print('Is the Hotel free for the event on given day?')
        return True

    def book_hotel(self):
        if self.__is_available():
            print('Registered the Booking\n\n')


class Florist(object):

    def __init__(self):
        print('Flower Decorations for the Event? --')

    @staticmethod
    def set_flower_requirements():
        print('Carnations, Roses and Lilies would be used for Decorations \n\n')


class Caterer(object):

    def __init__(self):
        print('Food Arrangements for the Event --')
        
    @staticmethod
    def set_cuisine():
        print('Chinese & Continental Cuisine to be served \n\n')


class Musician(object):

    def __init__(self):
        print('Musical Arrangements for the Marriage --')

    @staticmethod
    def set_music_type():
        print('Jazz and Classical will be played\n\n')


class You(object):

    def __init__(self):
        print('You:: Whoa! Marriage Arrangements??!!!')

    @staticmethod
    def ask_event_manager():
        print('You:: Let\'s Contact the Event Manager \n\n')
        em = EventManager()
        em.arrange()

    def __del__(self):
        print('You:: Thanks to Event Manager, all preparations done! Phew!')


you = You()
you.ask_event_manager()
