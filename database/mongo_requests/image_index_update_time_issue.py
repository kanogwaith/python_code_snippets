from pymongo import MongoClient

mongo = MongoClient('mongodb://green101:37017,green102:37017,green103:37017')

image_index = mongo['imageIndex']['ImageIndex']

for num, index in enumerate(image_index.find({'UpdateDate': {'$exists': True}})):
    if index['_updateTime'] < index['UpdateDate']:
        image_index.update_one({'_id': index['_id']}, {'$set': {'_updateTime': index['UpdateDate']}})
    print(num)

