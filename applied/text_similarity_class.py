from difflib import SequenceMatcher

from strsimpy.jaccard import Jaccard


class TextSimilarity(Jaccard):

    def __init__(self):
        Jaccard.__init__(self, 4)

    def jaccard_similarity(self, first: str, second: str) -> float:
        return self.similarity(first, second)

    @staticmethod
    def diff_similarity(first: str, second: str) -> float:
        return SequenceMatcher(None, first, second).ratio()
