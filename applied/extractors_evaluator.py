from pymongo import MongoClient
import requests

from site_indexer.package.article_extractors.boilerpipe_article_exractor import BoilerpipeArticleExtractor
from site_indexer.package.article_extractors.justtext_article_extractor import JustTextArticleExtractor
from site_indexer.package.article_extractors.NewsPapperArticleExtractor import NewsPapperTextArticleExtractor
from site_indexer.package.article_extractors.readability_article_extractor import ReadabilityArticleExtractor
from site_indexer.package.article_qualifier.similarity_checker import TextSimilarity


extractors = [
    BoilerpipeArticleExtractor,
    JustTextArticleExtractor,
    NewsPapperTextArticleExtractor,
    ReadabilityArticleExtractor
]

evaluator = TextSimilarity()

with MongoClient("mongodb://green101:37017,green102:37017,green103:37017") as mongo:
    coll = mongo['textExtractor']['Samples']
    for sample in coll.find({}, no_cursor_timeout=True):

        results_dict = {}
        with requests.get(sample['Url']) as response:
            if response.status_code == 200:
                for extractor_class in extractors:
                    article = extractor_class(response.text).get_article()
                    diff_name = 'diff_' + extractor_class.__name__
                    jacc_name = 'jacc_' + extractor_class.__name__
                    results_dict[diff_name] = evaluator.diff_similarity(sample['Body'], article)
                    results_dict[jacc_name] = evaluator.jaccard_similarity(sample['Body'], article)

        print(results_dict)
        if results_dict:
            coll.update_one({'_id': sample['_id']}, {'$set': results_dict})