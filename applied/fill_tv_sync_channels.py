from pymongo import MongoClient

mongo = MongoClient('mongodb://green101:37017,green102:37017,green103:37017')

settings_coll = mongo['audienceBuilderCache']['tv_sync_settings']


def get_max_seq():
    return max([i['seq_number'] for i in settings_coll.find()])


def get_server():
    data = {i: settings_coll.count_documents({'server_to_run': i}) for i in
            ['cloudera403', 'cloudera404', 'cloudera405', 'cloudera406']}
    return min(data, key=data.get)


def insert_new_record(url, name, account, note):
    settings_coll.insert_one({
        'seq_number': get_max_seq() + 1,
        'server_to_run': get_server(),
        'input': url,
        'channel_id': name,
        'region_id': '',
        'acc_no': account,
        'note': note
    })
