from difflib import SequenceMatcher

from strsimpy.jaccard import Jaccard


first = open('static_files/text_similarity/first').read()
second = open('static_files/text_similarity/second').read()

print(len(first), len(second))

jaccard = Jaccard(4)

print(jaccard.similarity(first, second))

diff = SequenceMatcher(None, first, second)
print(diff.ratio())
