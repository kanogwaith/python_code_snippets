from datetime import datetime

from pymongo import MongoClient


mongo = MongoClient('mongodb://green101:37017,green102:37017,green103:37017')

source_coll = mongo['irenInventory']['TextIndexDailyStatistic']
destination_coll = mongo['irenInventory']['UserStatistic']

for record in source_coll.find({'today': {'$lt': datetime(2020, 4, 26)}}):
    record.pop('_id')
    today = record.pop('today')
    print(today)

    for i in record:
        destination_coll.insert_one({
            'statistic_type': 'text',
            'create_date': today,
            'count': record[i],
            'keypass': i,
        })
