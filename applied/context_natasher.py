import logging
import datetime
from typing import Optional, List, Dict

import pysolr
import requests
from common_libs.database.models.hbase.hbase_field import HbaseField
from common_libs.database.models.hbase.tables.universal_channels import Channel
from common_libs.words_processing.stemmer import Stemmer
from happybase import ConnectionPool
from lxml.html import fromstring
from natasha import NamesExtractor
from pymongo import MongoClient


logging.basicConfig(
    format=u'%(filename)s[LINE:%(lineno)d]# %(levelname)-8s [%(asctime)s] --%(processName)s--  %(message)s',
    level=logging.INFO
)
logger = logging.getLogger(__name__)


class Natasher:
    zoo_host = "cloudera101:2181,cloudera102:2181,cloudera103:2181/solr"
    mongo_settings = {
        'uri': 'mongodb://green101:37017,green102:37017,green103:37017',
        'db': 'imageIndex',
        'collection': 'ImageIndex',
    }
    solr_collection = "compiled_index"
    hbase_host = "cloudera104"
    hbase_table = "channels"
    solr_query = {
        "defType": "edismax",
        "qf": 'art_t',
        "mm": 1,
        "stopwords": "true",
        "lowercaseOperators": "true",
        "sort": "id asc",
        "fl": "url",
        "wt": "json",
        "rows": 25000,
        "q.op": "AND",
        "shards.tolerant": "true"
    }

    channel_id = '126377'

    def __init__(self):
        self.channel = None

    def _stemmer(self, words):
        stemmed_words = list()
        lang = 'russian'
        stemmer = Stemmer(lang).get_stemmer()
        for word in words.lower().split(','):
            stemmed_words.append(stemmer.stem(word.strip()))
        return ','.join(stemmed_words)

    def _init_channel(self):
        pool = ConnectionPool(host=self.hbase_host, size=1)
        try:
            with pool.connection() as con:
                table = con.table(self.hbase_table)
                channel_data_exist_check = table.row(self.channel_id)
                if not channel_data_exist_check:
                    raise ValueError("Channel not found for id {0}".format(self.channel_id))
                else:
                    self.channel = Channel.init_from_native_hbase_object(channel_data_exist_check)
        except Exception:
            raise ValueError("Unable init channel {0} from hbase".format(self.channel_id))

    def _prepare_search_string(self):

        def channel_list_string_creator(field: HbaseField, stemmer: callable) -> str:
            result_string = ""
            if field is not None and field != "":
                stemmed_words = stemmer(self.channel.white_list)
                result_string = stemmed_words.replace(", ", ",")
                result_string = " ".join(result_string.split())
                result_string = result_string.replace(" ", "' and '")
                result_string = "(('" + result_string.replace(",", "') or ('") + "'))"
            return result_string
        white_str = channel_list_string_creator(self.channel.white_list, self._stemmer)
        black_str = channel_list_string_creator(self.channel.black_list, self._stemmer)

        domains_str = ""
        if self.channel.domains is not None and self.channel.domains != "":
            domains_str = self.channel.domains.replace(", ", ",")
            domains_str = "and (dom:" + domains_str.replace(",", " or dom:") + ")"

        query_str = "({0}{1}) {2}".format(white_str, black_str, domains_str)
        return query_str

    @staticmethod
    def get_solr_compatible_date_range(initial_time: datetime.datetime, minutes_count):
        date_fmt = "%Y-%m-%dT%H:%M:%S:00Z"
        time_back = (initial_time - datetime.timedelta(minutes=minutes_count))
        day_timestamp = int(initial_time.timestamp())
        time_range = '[{} TO {}]'.format(time_back.strftime(date_fmt), initial_time.strftime(date_fmt))
        return [time_range, day_timestamp]

    def _request_to_solr(self, zoo_host, collection_name, search_string, date_range):
        """"""
        zookeeper = pysolr.ZooKeeper(zoo_host)
        collection = pysolr.SolrCloud(zookeeper, collection_name)
        logger.info("Start solr requests")

        date_field = "lst_mdf"

        req_date, timestamp = date_range

        old_cursor_mark = "old"
        cursor_mark = "*"
        self.solr_query["fq"] = date_field + ":" + req_date
        while old_cursor_mark != cursor_mark:
            self.solr_query["cursorMark"] = cursor_mark
            logger.debug("Start subrequest for: \"{0}\", query_opt: {1}".format(search_string, self.solr_query))
            response = collection.search(search_string, **self.solr_query)
            old_cursor_mark = cursor_mark
            cursor_mark = response.nextCursorMark if response.nextCursorMark else old_cursor_mark
            yield response.docs

    @staticmethod
    def get_state_title(url: str) -> Optional[str]:

        with requests.get(url) as response:
            if response.status_code == 200:
                return fromstring(response.content).findtext('.//title')

    @staticmethod
    def title_natashing(title: str) -> List[str]:
        extractor = NamesExtractor()
        matches = extractor(title)
        result = []
        for i in [[i for i in match.fact if i] for match in matches]:
            result.extend(i)
        return result

    def write_additional_data_to_mongo(self, solr_data: List[Dict[str, str]]) -> None:
        mongo = MongoClient(self.mongo_settings['uri'])
        coll = mongo[self.mongo_settings['db']][self.mongo_settings['collection']]
        for url_dict in solr_data:
            try:
                title = self.get_state_title(url_dict['url'])
            except:
                continue
            try:
                natashed = self.title_natashing(title)
            except AttributeError:
                continue
            if natashed:
                query = {'$set': {'AdditionalData': {'TitleProperNames': natashed}}}
                updated_records = coll.find({'PageUrls': {'$in': [url_dict['url']]}}, no_cursor_timeout=True)
                for record in updated_records:
                    query['$set']['_updateTime'] = datetime.datetime.now()
                    coll.update_one({'_id': record['_id']}, query)
                    logger.info('record {} updated'.format(record['_id']))

    def write_to_mongo_alternative(self, solr_data: List[Dict[str, str]]) -> None:
        with MongoClient(self.mongo_settings['uri']) as mongo:
            coll = mongo[self.mongo_settings['db']][self.mongo_settings['collection']]
            for i in solr_data:
                try:
                    title = self.get_state_title(i['url'])
                except:
                    continue
                natashed = self.title_natashing(title)
                i['natash'] = natashed

            urls_dict = {i['url']: i['natash'] for i in solr_data if i['natash']}

            start_t = datetime.datetime.now()
            logger.info('Request started')
            long_request = coll.find({'PageUrls': {'$in': list(urls_dict.keys())}},
                                     no_cursor_timeout=True)
            logger.info('Request count {} seconds'.format((datetime.datetime.now() - start_t).total_seconds()))
            for record in long_request:
                for url in record['PageUrls']:
                    if url in urls_dict:
                        query = {'$set': {'AdditionalData': {'TitleProperNames': urls_dict[url],
                                                             '_updateTime': datetime.datetime.now()}}}
                        coll.update_one({'_id': record['_id']}, query)
                        logger.info('record {} updated'.format(record['_id']))

    def run(self):
        self._init_channel()
        search_string = self._prepare_search_string()
        logger.info(search_string)
        date_range = self.get_solr_compatible_date_range(datetime.datetime.now(), 60*24*14)
        solr_request_generator = self._request_to_solr(zoo_host=self.zoo_host,
                                                       collection_name=self.solr_collection,
                                                       search_string=search_string,
                                                       date_range=date_range)
        for batch in solr_request_generator:
            self.write_additional_data_to_mongo(batch)


context_natasher = Natasher()
context_natasher.run()
