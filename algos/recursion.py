from pymongo import MongoClient, ASCENDING
from pprint import pprint
from bson.objectid import ObjectId

MONGO_HOST = "mongodb://green101:37017,green102:37017,green103:37017"

IREN_INTENTORY_DB = 'irenInventory'
IREN_USERDATA_COLL = 'users'
IREN_IMAGE_COUNTERS_COLL = 'ImagesUploadStatistic'
IREN_TEXT_COUNTERS_COLL = 'TextIndexDailyStatistic'

TEXT_CATEGORIES_COLL = 'TextCategories'
TEXT_INDEX_COLL = 'TextIndex'

def process_node(node, child_structure, full_col):
    if node['_id'] in child_structure:
        node['children'] = [
            process_node(
                {'_id': i, 'name': full_col[i]['name']}, child_structure, full_col
            ) for i in child_structure[node['_id']]
        ]
        return node
    else:
        return {'_id': node['_id'], 'name': full_col[node['_id']]['name']}


with MongoClient(MONGO_HOST) as mongo:
    text_categories_coll = mongo[IREN_INTENTORY_DB][TEXT_CATEGORIES_COLL]

    full_collection = {
        i['_id']: {'name': i['Name']} for i in text_categories_coll.find()
    }
    node_stack = [{'_id': i['_id'], 'name': i['Name']} for i in text_categories_coll.find(
        {'ParentId': {'$exists': False}}
    ).sort('Name', ASCENDING)]

    child_struct = {i['_id']: i['children_ids'] for i in text_categories_coll.aggregate([{
        '$group':
            {
                '_id': '$ParentId',
                'children_ids': {'$addToSet': '$_id'}
            }
    }])}

    pprint([process_node(i, child_struct, full_collection) for i in node_stack])
