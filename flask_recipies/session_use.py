from flask import session, request
from flask_restplus import fields, api, Resource
from flask_restplus.reqparse import RequestParser

some = api.model('', {'a': fields.String()})
parser = RequestParser()
parser.add_argument('a')

@ns.route('/session')
class SessionMakeup(Resource):

    @api.marshal_with(some)
    def get(self):
        return {'a': session.get('some')}

    @api.expect(parser)
    def post(self):
        session['some'] = request.args['a']
