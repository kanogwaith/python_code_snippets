from __future__ import print_function
import pickle
import os.path
from googleapiclient.discovery import build
from google_auth_oauthlib.flow import InstalledAppFlow
from google.auth.transport.requests import Request
from pprint import pprint
import datetime

from pymongo import MongoClient

SCOPES = ['https://www.googleapis.com/auth/spreadsheets.readonly']
basic_id = '179OJZA4aGAIPd1pieb_OGQg5k5IdL57ugJDj6n5YQXY'
sheet_name = 'Лист1!A:B'

def get_credentials():
    """Shows basic usage of the Sheets API.
    Prints values from a sample spreadsheet.
    """
    creds = None
    # The file token.pickle stores the user's access and refresh tokens, and is
    # created automatically when the authorization flow completes for the first
    # time.
    if os.path.exists('./static_files/token.pickle'):
        with open('./static_files/token.pickle', 'rb') as token:
            creds = pickle.load(token)
    # If there are no (valid) credentials available, let the user log in.
    if not creds or not creds.valid:
        if creds and creds.expired and creds.refresh_token:
            creds.refresh(Request())
        else:
            flow = InstalledAppFlow.from_client_secrets_file(
                './static_files/credentials.json', SCOPES)
            creds = flow.run_local_server(port=0)
        # Save the credentials for the next run
        with open('./static_files/token.pickle', 'wb') as token:
            pickle.dump(creds, token)

    return creds


def get_values(creds, sheet_id, sheet_name):

    service = build('sheets', 'v4', credentials=creds)
    sheet = service.spreadsheets()
    result = sheet.values().get(spreadsheetId=sheet_id, range=sheet_name).execute()
    values = result.get('values', [])
    return values


creds = get_credentials()

with MongoClient('mongodb://green101:37017,green102:37017,green103:37017') as mongo:
    for i in get_values(creds, basic_id, sheet_name):
        name = i[0]
        # urls = [i[0] for i in get_values(creds, i[1].split('/')[-2], 'Лист1!A:A') if i]
        # for url in urls:
        #     mongo['imageIndex']['DatasetIndex'].insert_one({
        #         'DatasetName': name,
        #         'DataSource': 'clients',
        #         'Url': url
        #     })
        #     print(name, url)

        mongo['imageIndex']['ImageDatasetsCategories'].insert_one({
            'EnName': name,
            'UpdateDate1': datetime.datetime.now()
        })
