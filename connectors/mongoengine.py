from datetime import datetime

from marshmallow import Schema
from mongoengine import connect, Document, DateTimeField, StringField, BooleanField, ListField

connect(db='irenInventory', host='mongodb://green101:37017,green102:37017,green103:37017')


class TextIndex(Document):
    create_date = DateTimeField(db_field='CreateDate', required=True)
    url = StringField(db_field='Url')
    is_watched = BooleanField(db_field='IsWatched', default=False)
    categories = ListField(db_field='Categories', default=[])

    meta = {'collection': 'TextIndex'}


new_record = TextIndex(create_date=datetime.now(), url='test_url', is_watched=True)
new_record.save()

print(TextIndex.objects(url='test_url').first().to_json())
print('\n\n---\n\n')
print(TextIndex.objects(url='test_url').first().create_date)
print('\n\n---\n\n')
for i in TextIndex.objects(is_watched=True)[0:10]:
    print(i.id)

new_record.delete()
assert(TextIndex.objects(url='test_url').first() is None)