from pykafka import KafkaClient
import json


def kafka_generator():
    client = KafkaClient(zookeeper_hosts="cloudera101:2181,cloudera102:2181,cloudera103:2181/ram")
    topic = client.topics[b'druid_queue']
    consumer = topic.get_balanced_consumer(consumer_group=b'test_no_solr_consumer', managed=True)

    for r in consumer:
        decoded_mes = r.value.decode("utf8")
        mes = json.loads(decoded_mes)
        url = mes.get("url")

        if url is not None:

            yield url
