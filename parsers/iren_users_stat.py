import datetime

from pymongo import MongoClient, ASCENDING


mongo = MongoClient('mongodb://green101:37017,green102:37017,green103:37017')


def get_for_last_n_days(days_count):
    users = [i for i in mongo['irenInventory']['users'].find()]

    def get_mongo_payload(coll_name: str) -> list:
        return [i for i in mongo['irenInventory'][coll_name].find(
            {
                'today': {
                    '$lte': datetime.datetime.now() + datetime.timedelta(days=1),
                    '$gt': datetime.datetime.now() - datetime.timedelta(days=days_count)
                }
            }).sort('today', ASCENDING)]

    images_stat = get_mongo_payload('ImagesUploadStatistic')

    text_index_stat = get_mongo_payload('TextIndexDailyStatistic')

    headers = ['User']
    headers.extend([(datetime.datetime.now() - datetime.timedelta(days=days_count - i)).strftime('%d-%m') for i in range(1, days_count + 1)])

    def write_to_file_table_info(data: list):
        f.write(('|{:20}' * len(headers) + '\n' + '-' * 211 + '\n').format(*headers))
        for i in users:
            user_array = [i['login']]
            user_array.extend([j[i['pass']] for j in data])
            f.write(('|{:20}' * len(user_array) + '\n').format(*user_array))

    with open('../tmp/image_stats.txt', 'w') as f:

        f.write('IMAGES UPLOADS\n' + '-' * 211 + '\n')

        write_to_file_table_info(images_stat)

        f.write('TEXT INDEX UPLOADS\n' + '-' * 211 + '\n')

        write_to_file_table_info(text_index_stat)

get_for_last_n_days(5)